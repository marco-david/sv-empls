(* ::Package:: *)

(************************************************************************)
(* This file was generated automatically by the Mathematica front end.  *)
(* It contains Initialization cells from a Notebook file, which         *)
(* typically will have the same name as this file except ending in      *)
(* ".nb" instead of ".m".                                               *)
(*                                                                      *)
(* This file is intended to be loaded into the Mathematica kernel using *)
(* the package loading commands Get or Needs.  Doing so is equivalent   *)
(* to using the Evaluate Initialization Cells menu command in the front *)
(* end.                                                                 *)
(*                                                                      *)
(* DO NOT EDIT THIS FILE.  This entire file is regenerated              *)
(* automatically each time the parent Notebook file is saved in the     *)
(* Mathematica front end.  Any changes you make to this file will be    *)
(* overwritten.                                                         *)
(************************************************************************)



(* ::Input::Initialization:: *)
Now


(* ::Input::Initialization:: *)
(* Formatting complex conjugates \[Dash] code from https://mathematica.stackexchange.com/questions/268066/display-conjugatex-as-a-long-overbar-permanently *)
Unprotect[TemplateBox];
TemplateBox[x_,"Conjugate",SyntaxForm->SuperscriptBox]:=OverscriptBox[PaneBox[x[[1]],ImageMargins->1],"_"];
Protect[TemplateBox];


(* ::Input::Initialization:: *)
\[Tau]::usage="The modular parameter \[Tau].";
q::usage="Symbol denoting q = Exp[2\[Pi]I\[Tau]].";
z::usage="Symbol reserved for denoting a puncture.";
t::usage ="Symbol denoting t = Exp[2\[Pi]Iz].";
u::usage="Symbol reserved for the relation z = u\[Tau]+v.";
v::usage="Symbol reserved for the relation z = u\[Tau]+v.";

Replaceu := ReplaceAll[u:>(Log[Abs[t]]/Log[Abs[q]])];
Replace\[Tau]z := ReplaceAll[{\[Tau]:>(1/(2 Pi I) Log[q]),z:>(1/(2 Pi I) Log[t])}];
Replacetq := ReplaceAll[{Log[Exp[x_]]:>x}]@*ReplaceAll[{q -> Exp[2 Pi I \[Tau]], t -> Exp[2 Pi I z]}];
Replacetqu := Replacetq @*Replaceu;


(* ::Input::Initialization:: *)
\[Theta]1[z_,\[Tau]_]:=EllipticTheta[1,\[Pi] z,E^(I \[Pi] \[Tau])];
\[Theta]1'[z_,\[Tau]_]:=\[Pi] EllipticThetaPrime[1,\[Pi] z,E^(I \[Pi] \[Tau])];
F[z_,\[Alpha]_,\[Tau]_]:=(\[Theta]1'[0,\[Tau]]\[Theta]1[z+\[Alpha],\[Tau]])/( \[Theta]1[z,\[Tau]]\[Theta]1[\[Alpha],\[Tau]]);
gEv[0, z_, \[Tau]_]:= 1;
g[0, z_, \[Tau]_]:= 1;
gEv[n_, z_, \[Tau]_]:=CoefficientList[Series[F[z,\[Alpha],\[Tau]]-1/\[Alpha],{\[Alpha], 0,n}],{\[Alpha]}][[n]]/.Derivative[0,k_,0][EllipticThetaPrime][1,0,x_]/; OddQ[k] -> 0; (* 1-based indexing accounts for the offset g^(k-1) in the series expansion; as Subscript[\[Theta], 1] is odd, all it's odd derivatives evaluate to 0 at z=0. *)

Format[g[n_,z_, \[Tau]_], TraditionalForm] := DisplayForm[RowBox[{SuperscriptBox["g",RowBox[{"(",MakeBoxes[n,TraditionalForm], ")"}]], "(", MakeBoxes[z,TraditionalForm], ",", MakeBoxes[\[Tau], TraditionalForm], ")"}]]
Evalg := ReplaceAll[{g[a_,b_,c_]:>gEv[a,b,c]}]


(* ::Input::Initialization:: *)
(* Evaluation Definitions *)
eMPLEv[{{},{}},z_, \[Tau]_]:=1;
eMPL[{{},{}},z_, \[Tau]_]:=1;
eMPLEv[a_, z_, \[Tau]_]/;MatrixQ[a]&&MatchQ[Transpose[a][[1]],{_Integer, _}]:=Integrate[g[a[[1,1]],Subscript[x, Length[a[[1]]]]-a[[2,1]], \[Tau]]eMPL[a[[All, 2;;-1]], Subscript[x, Length[a[[1]]]], \[Tau]], {Subscript[x, Length[a[[1]]]], 0, z}];

(* Symbolic Differentiation *)
eMPL/:Derivative[eMPL[a_, z, \[Tau]],z] /;MatrixQ[a]&&MatchQ[Transpose[a][[1]],{_Integer, _}]:=g[a[[1,1]],z-a[[2,1]], \[Tau]]eMPL[a[[All, 2;;-1]], z, \[Tau]];
Derivative[x_,1,0][eMPL][a_, z, \[Tau]] /;MatrixQ[a]&&MatchQ[Transpose[a][[1]],{_Integer, _}]&&MatrixQ[x,MatchQ[#,0]&]:= g[a[[1,1]],z-a[[2,1]], \[Tau]]eMPL[a[[All, 2;;-1]], z, \[Tau]] ;

(* Symbolic Manipulation & Formatting *)
\!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[x___,z_,\[Tau]_]:=eMPL[x, z, \[Tau]];
Format[eMPL[x___, z_, \[Tau]_],TraditionalForm]:=DisplayForm[Style[RowBox[{
"\!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)","(",TableForm[x/.Null -> "",TableSpacing->{1,1}],";",MakeBoxes[z, TraditionalForm], ",", MakeBoxes[\[Tau], TraditionalForm],")"}],SpanMaxSize->Infinity]];

EvaleMPL := ReplaceRepeated[{eMPL->eMPLEv}];


(* ::Input::Initialization:: *)
Subscript[\[Omega], n_][m_, \[Tau]_]/;n>=1&&IntegerQ[n]:=\[Omega][m,Sequence@@ConstantArray[0, n-1], \[Tau]]
\[Omega]Ev[n__, \[Tau]_]:= eMPLEv[{Reverse@List@n, ConstantArray[0, Length@List@n]}, 1, \[Tau]];
Format[\[Omega][n__,\[Tau]_], TraditionalForm] := DisplayForm[RowBox[{"\[Omega]","(",StringRiffle[List@n, ","], ";",  MakeBoxes[\[Tau], TraditionalForm],")"}]]
EvaleMZV :=ReplaceRepeated[{\[Omega]->\[Omega]Ev}];

eZVarg[n_, m_]/;n>= 1:=ArrayFlatten[{{ConstantArray[0, {2,n-1}], {
 {m},
 {0}
}}}];


(* ::Input::Initialization:: *)
DabEv[a_,b_,t_]:=(-1)^(b-1) \!\(
\*UnderoverscriptBox[\(\[Sum]\), \(n = b\), \(a + b - 1\)]\(Binomial[n - 1, b - 1]\ 
\*FractionBox[
SuperscriptBox[\((\(-2\)\ Log[Abs[t]])\), \(a + b - n - 1\)], \(\((a + b - n - 1)\)!\)] Conjugate[PolyLog[n, t]]\)\)+(-1)^(a-1) \!\(
\*UnderoverscriptBox[\(\[Sum]\), \(n = a\), \(a + b - 1\)]\(Binomial[n - 1, a - 1] 
\*FractionBox[
SuperscriptBox[\((\(-2\)\ Log[Abs[t]])\), \(a + b - n - 1\)], \(\((a + b - n - 1)\)!\)] PolyLog[n, t]\)\);
Format[Dab[a_,b_,t_], TraditionalForm] := Subscript[D, a,b][t];


(* ::Input::Initialization:: *)
d[k_] := Piecewise[{{-1, k==1}, {0, EvenQ[k]}, {-Sum[d[2j+1]/(k-2j)!, {j, 0, (k-3)/2}],OddQ[k]}}]


(* ::Input::Initialization:: *)
P[n_, z_, q_]/;n>=1 :=(2 Pi I)^(n-1) Sum[Sum[d[2k+1]Subscript[\[Omega], 2j+2-2k][1,\[Tau]] z^(n-1-2j)/(n-1-2j)!,{k,0,j}], {j, 0, Floor[(n-1)/2]}]


(* ::Input::Initialization:: *)
En1[0,t_,q_] :=1/(2Pi I) g[1,z, \[Tau]];
En1[1,t_,q_] :=\!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[{
 {1},
 {0}
},z,\[Tau]]-Subscript[\[Omega], 2][1, \[Tau]];
En1[n_/;n>=2 ,t_,q_]:=(2 Pi I)^(n-1) \!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[eZVarg[n, 1], z,\[Tau]] + P[n, z, q];


(* ::Input::Initialization:: *)
Enm1Ev[n_,0, t_, q_]/;n>=0:=En1[n,t, q] +(1/2 PolyLog[n, t]-(-1)^n 1/2 PolyLog[n, t^-1]);
Enm1Ev[0, m_, t_, q_]/;m<0:= ReplaceAll[\!\(\*OverscriptBox[\(m\), \(~\)]\) -> -m][\!\(\*OverscriptBox[\(m\), \(~\)]\)!/(2Pi I)^(\!\(\*OverscriptBox[\(m\), \(~\)]\)+1) (g[\!\(\*OverscriptBox[\(m\), \(~\)]\)+1,z,\[Tau]]+(1+(-1)^(\!\(\*OverscriptBox[\(m\), \(~\)]\)+1))Subscript[\[Zeta], \!\(\*OverscriptBox[\(m\), \(~\)]\)+1])];
Enm1Ev[n_, m_, t_, q_]/;n>=1&&m<0 && OddQ[m]:=ReplaceAll[\!\(\*OverscriptBox[\(m\), \(~\)]\)-> -m][\!\(\*OverscriptBox[\(m\), \(~\)]\)! (2Pi I)^(n-1-\!\(\*OverscriptBox[\(m\), \(~\)]\)) (\!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[eZVarg[n,\!\(\*OverscriptBox[\(m\), \(~\)]\)+1],z,\[Tau]]+Sum[Sum[d[2k+1]Subscript[\[Omega], 2j+1-2k][\!\(\*OverscriptBox[\(m\), \(~\)]\)+1,\[Tau]] z^(n-2j)/(n-2j)!,{k,0,j}],{j,0, Floor[n/2]}]) ];
Enm1Ev[n_, m_, t_, q_] /;n>=1 &&m<0&& EvenQ[m]:=ReplaceAll[\!\(\*OverscriptBox[\(m\), \(~\)]\)->-m][ \!\(\*OverscriptBox[\(m\), \(~\)]\)! (2Pi I)^(n-1-\!\(\*OverscriptBox[\(m\), \(~\)]\)) (\!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[eZVarg[n,\!\(\*OverscriptBox[\(m\), \(~\)]\)+1],z,\[Tau]]+Sum[Sum[d[2k+1]Subscript[\[Omega], 2j+2-2k][\!\(\*OverscriptBox[\(m\), \(~\)]\)+1,\[Tau]] z^(n-1-2j)/(n-1-2j)!,{k,0,j}],{j,0, Floor[(n-1)/2]}])];
Format[Enm1[n_,m_,t_,q_], TraditionalForm] := Subscript["E", n,m][t,q];
EvalEnm1 = ReplaceAll[{Enm1 -> Enm1Ev}];

(* Differential equation defining the Subscript[E, n,m] *)
Enm1/:Derivative[Enm1[n_,m_,t,q], z]/;n>=1&&m<0:=2 Pi I Enm1[n-1, m,t,q];
Derivative[0,0,1,0][Enm1][n_,m_,t_,q_] /;n>=1&&m<0:=1/t Enm1[n-1, m,t,q];


(* ::Input::Initialization:: *)
DEab[a_, b_, t_, q_] := (-1)^a Sum[Binomial[n-1, a-1] (-2)^(a+b-1-n)/(a+b-1-n)! Sum[Binomial[a+b-1-n, m]Log[Abs[t]]^(a+b-1-n-m) Log[Abs[q]]^m Enm1[n, -m, t, q],{m, 0, a+b-1-n}],{n, a, a+b-1}]+(-1)^b Sum[Binomial[n-1, b-1] (-2)^(a+b-1-n)/(a+b-1-n)! Sum[Binomial[a+b-1-n, m]Log[Abs[t]]^(a+b-1-n-m) Log[Abs[q]]^m Conjugate[Enm1[n, -m, t, q]],{m,0,a+b-1-n}],{n,b,a+b-1}]+ Dab[a,b,t]+(-2Log[Abs[q]])^(a+b-1)/(a+b)! BernoulliB[a+b, u]


(* ::Input::Initialization:: *)
ToeMPL = ReplaceAll[{\!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[a_, Y, b_]:>\!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[a, z, b],g[n_, Y, b_]:>g[n,z,b]}]@*ReplaceAll[{z:>\!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[eZVarg[1,0], z, \[Tau]],z^n_:> n! \!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[eZVarg[n,0], z, \[Tau]], Conjugate[z]^n_:> n!Conjugate[ \!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[eZVarg[n,0], z, \[Tau]]]}]@*ReplaceAll[{\!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[a_, z, b_]:>\!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[a, Y, b],g[n_, z, b_]:>g[n,Y,b]}]@*ExpandAll@*ReplaceAll[{Im[z]:>1/(2I) (z -Conjugate[z]),Re[z]:>1/2 (z+Conjugate[z])}];
ToZPoly = ReplaceAll[{\!\(\*OverscriptBox[\(\[CapitalGamma]\), \(~\)]\)[eZVarg[n_,0]; z, \[Tau]]:> z^n/n!}];


(* ::Input::Initialization:: *)
CollectIm\[Tau] = Collect[ExpandAll[#],Im[\[Tau]], Simplify]&;
CollectIm\[Tau]Expand = Collect[ExpandAll[#],Im[\[Tau]]]&;
CustomSimplify = Simplify[#,ExcludedForms->{Im[\[Tau]]}]&;
ReplaceImRe[e_] := ReplaceAll[e,{Re[x_]:>1/2 (x+Conjugate[x]),Im[x_]:>1/(2I) (x - Conjugate[x])}];
CustomFullSimplify = FullSimplify[#,TransformationFunctions->{Automatic,ReplaceImRe}]&;


(* ::Input::Initialization:: *)
EvalLiInv = ReplaceAll[{PolyLog[n_,1/t_]:>(-1)^n (-PolyLog[n, t] - (2Pi I)^n/n! BernoulliB[n,Log[t]/(2 Pi I)]) }];


(* ::Input::Initialization:: *)
Derivative[1][Conjugate][x_]:=0;
Derivative[1][Re][x_]:=1/2;
Derivative[1][Im][x_]:=1/(2I);


(* ::Input::Initialization:: *)
Simplifytq=(#/.Dab ->DabEv//EvalEnm1//EvalLiInv)&;
Simplify\[Tau]z =(#//Replacetqu//CustomSimplify//CollectIm\[Tau])&;


(* ::Input::Initialization:: *)
DE11tq = DEab[1,1, t, q]//Simplifytq;
DE11tq // TraditionalForm


(* ::Input::Initialization:: *)
DE11\[Tau]z = DE11tq//CustomFullSimplify//Simplify\[Tau]z;
DE11\[Tau]z//TraditionalForm



